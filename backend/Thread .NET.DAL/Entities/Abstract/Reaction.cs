﻿namespace Thread_.NET.DAL.Entities.Abstract
{
    public enum ReactionMark
    {
        Like,
        Dislike,
        Restricted,
    }
    public abstract class Reaction : BaseEntity
    {
        public int UserId { get; set; }
        public User User { get; set; }

        public ReactionMark ReactionMark { get; set; } = ReactionMark.Restricted;

    }
}
