﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Thread_.NET.DAL.Migrations
{
    public partial class Dislikes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DropColumn(
                name: "IsLike",
                table: "PostReactions");

            migrationBuilder.DropColumn(
                name: "IsLike",
                table: "CommentReactions");

            migrationBuilder.AddColumn<int>(
                name: "ReactionMark",
                table: "PostReactions",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ReactionMark",
                table: "CommentReactions",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.InsertData(
                table: "CommentReactions",
                columns: new[] { "Id", "CommentId", "CreatedAt", "ReactionMark", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 1, 6, new DateTime(2021, 6, 15, 22, 16, 3, 834, DateTimeKind.Local).AddTicks(1920), 0, new DateTime(2021, 6, 15, 22, 16, 3, 834, DateTimeKind.Local).AddTicks(2473), 6 },
                    { 18, 12, new DateTime(2021, 6, 15, 22, 16, 3, 834, DateTimeKind.Local).AddTicks(3518), 1, new DateTime(2021, 6, 15, 22, 16, 3, 834, DateTimeKind.Local).AddTicks(3522), 21 },
                    { 17, 6, new DateTime(2021, 6, 15, 22, 16, 3, 834, DateTimeKind.Local).AddTicks(3488), 0, new DateTime(2021, 6, 15, 22, 16, 3, 834, DateTimeKind.Local).AddTicks(3493), 1 },
                    { 15, 17, new DateTime(2021, 6, 15, 22, 16, 3, 834, DateTimeKind.Local).AddTicks(3426), 1, new DateTime(2021, 6, 15, 22, 16, 3, 834, DateTimeKind.Local).AddTicks(3430), 8 },
                    { 14, 4, new DateTime(2021, 6, 15, 22, 16, 3, 834, DateTimeKind.Local).AddTicks(3397), 0, new DateTime(2021, 6, 15, 22, 16, 3, 834, DateTimeKind.Local).AddTicks(3401), 2 },
                    { 13, 7, new DateTime(2021, 6, 15, 22, 16, 3, 834, DateTimeKind.Local).AddTicks(3366), 2, new DateTime(2021, 6, 15, 22, 16, 3, 834, DateTimeKind.Local).AddTicks(3370), 19 },
                    { 12, 2, new DateTime(2021, 6, 15, 22, 16, 3, 834, DateTimeKind.Local).AddTicks(3336), 2, new DateTime(2021, 6, 15, 22, 16, 3, 834, DateTimeKind.Local).AddTicks(3340), 8 },
                    { 11, 15, new DateTime(2021, 6, 15, 22, 16, 3, 834, DateTimeKind.Local).AddTicks(3306), 2, new DateTime(2021, 6, 15, 22, 16, 3, 834, DateTimeKind.Local).AddTicks(3310), 10 },
                    { 19, 18, new DateTime(2021, 6, 15, 22, 16, 3, 834, DateTimeKind.Local).AddTicks(3548), 0, new DateTime(2021, 6, 15, 22, 16, 3, 834, DateTimeKind.Local).AddTicks(3552), 20 },
                    { 10, 19, new DateTime(2021, 6, 15, 22, 16, 3, 834, DateTimeKind.Local).AddTicks(3275), 1, new DateTime(2021, 6, 15, 22, 16, 3, 834, DateTimeKind.Local).AddTicks(3279), 13 },
                    { 8, 15, new DateTime(2021, 6, 15, 22, 16, 3, 834, DateTimeKind.Local).AddTicks(3214), 0, new DateTime(2021, 6, 15, 22, 16, 3, 834, DateTimeKind.Local).AddTicks(3219), 8 },
                    { 7, 18, new DateTime(2021, 6, 15, 22, 16, 3, 834, DateTimeKind.Local).AddTicks(3184), 1, new DateTime(2021, 6, 15, 22, 16, 3, 834, DateTimeKind.Local).AddTicks(3188), 17 },
                    { 6, 17, new DateTime(2021, 6, 15, 22, 16, 3, 834, DateTimeKind.Local).AddTicks(3152), 2, new DateTime(2021, 6, 15, 22, 16, 3, 834, DateTimeKind.Local).AddTicks(3156), 18 },
                    { 5, 16, new DateTime(2021, 6, 15, 22, 16, 3, 834, DateTimeKind.Local).AddTicks(3109), 0, new DateTime(2021, 6, 15, 22, 16, 3, 834, DateTimeKind.Local).AddTicks(3116), 12 },
                    { 4, 14, new DateTime(2021, 6, 15, 22, 16, 3, 834, DateTimeKind.Local).AddTicks(3015), 0, new DateTime(2021, 6, 15, 22, 16, 3, 834, DateTimeKind.Local).AddTicks(3019), 7 },
                    { 3, 14, new DateTime(2021, 6, 15, 22, 16, 3, 834, DateTimeKind.Local).AddTicks(2981), 1, new DateTime(2021, 6, 15, 22, 16, 3, 834, DateTimeKind.Local).AddTicks(2986), 21 },
                    { 2, 7, new DateTime(2021, 6, 15, 22, 16, 3, 834, DateTimeKind.Local).AddTicks(2934), 0, new DateTime(2021, 6, 15, 22, 16, 3, 834, DateTimeKind.Local).AddTicks(2942), 21 },
                    { 9, 12, new DateTime(2021, 6, 15, 22, 16, 3, 834, DateTimeKind.Local).AddTicks(3245), 1, new DateTime(2021, 6, 15, 22, 16, 3, 834, DateTimeKind.Local).AddTicks(3249), 13 },
                    { 20, 8, new DateTime(2021, 6, 15, 22, 16, 3, 834, DateTimeKind.Local).AddTicks(3577), 1, new DateTime(2021, 6, 15, 22, 16, 3, 834, DateTimeKind.Local).AddTicks(3582), 11 },
                    { 16, 8, new DateTime(2021, 6, 15, 22, 16, 3, 834, DateTimeKind.Local).AddTicks(3456), 1, new DateTime(2021, 6, 15, 22, 16, 3, 834, DateTimeKind.Local).AddTicks(3460), 5 }
                });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 20, "Assumenda omnis voluptatem qui molestiae.", new DateTime(2021, 6, 15, 22, 16, 3, 821, DateTimeKind.Local).AddTicks(8439), 2, new DateTime(2021, 6, 15, 22, 16, 3, 821, DateTimeKind.Local).AddTicks(9101) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 20, "Recusandae hic id dolorum expedita.", new DateTime(2021, 6, 15, 22, 16, 3, 821, DateTimeKind.Local).AddTicks(9846), 16, new DateTime(2021, 6, 15, 22, 16, 3, 821, DateTimeKind.Local).AddTicks(9855) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 20, "Rerum saepe commodi.", new DateTime(2021, 6, 15, 22, 16, 3, 821, DateTimeKind.Local).AddTicks(9934), 1, new DateTime(2021, 6, 15, 22, 16, 3, 821, DateTimeKind.Local).AddTicks(9940) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 9, "Consequatur aut et est facilis sint adipisci.", new DateTime(2021, 6, 15, 22, 16, 3, 822, DateTimeKind.Local).AddTicks(4), 18, new DateTime(2021, 6, 15, 22, 16, 3, 822, DateTimeKind.Local).AddTicks(10) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Animi laudantium omnis nobis consequuntur voluptas nihil facere aut dolore.", new DateTime(2021, 6, 15, 22, 16, 3, 822, DateTimeKind.Local).AddTicks(80), 2, new DateTime(2021, 6, 15, 22, 16, 3, 822, DateTimeKind.Local).AddTicks(85) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "Rerum iure et quia.", new DateTime(2021, 6, 15, 22, 16, 3, 822, DateTimeKind.Local).AddTicks(132), 5, new DateTime(2021, 6, 15, 22, 16, 3, 822, DateTimeKind.Local).AddTicks(137) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { "Id ut voluptatem nesciunt amet ut eum nihil.", new DateTime(2021, 6, 15, 22, 16, 3, 822, DateTimeKind.Local).AddTicks(197), 4, new DateTime(2021, 6, 15, 22, 16, 3, 822, DateTimeKind.Local).AddTicks(201) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 1, "Excepturi architecto fuga.", new DateTime(2021, 6, 15, 22, 16, 3, 822, DateTimeKind.Local).AddTicks(350), 4, new DateTime(2021, 6, 15, 22, 16, 3, 822, DateTimeKind.Local).AddTicks(355) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "Aut aut et rem laborum hic.", new DateTime(2021, 6, 15, 22, 16, 3, 822, DateTimeKind.Local).AddTicks(407), 16, new DateTime(2021, 6, 15, 22, 16, 3, 822, DateTimeKind.Local).AddTicks(411) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "Illum sit suscipit omnis impedit.", new DateTime(2021, 6, 15, 22, 16, 3, 822, DateTimeKind.Local).AddTicks(459), 4, new DateTime(2021, 6, 15, 22, 16, 3, 822, DateTimeKind.Local).AddTicks(463) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 11, "Labore harum sed qui.", new DateTime(2021, 6, 15, 22, 16, 3, 822, DateTimeKind.Local).AddTicks(505), 16, new DateTime(2021, 6, 15, 22, 16, 3, 822, DateTimeKind.Local).AddTicks(510) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Porro rem et eos voluptas maiores provident eos adipisci rem.", new DateTime(2021, 6, 15, 22, 16, 3, 822, DateTimeKind.Local).AddTicks(576), 13, new DateTime(2021, 6, 15, 22, 16, 3, 822, DateTimeKind.Local).AddTicks(580) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 5, "Quaerat consequuntur sit rem iure aut non.", new DateTime(2021, 6, 15, 22, 16, 3, 822, DateTimeKind.Local).AddTicks(632), 8, new DateTime(2021, 6, 15, 22, 16, 3, 822, DateTimeKind.Local).AddTicks(636) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Laudantium provident ipsam non eum reprehenderit maiores.", new DateTime(2021, 6, 15, 22, 16, 3, 822, DateTimeKind.Local).AddTicks(781), 13, new DateTime(2021, 6, 15, 22, 16, 3, 822, DateTimeKind.Local).AddTicks(786) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Cum facere eos ratione.", new DateTime(2021, 6, 15, 22, 16, 3, 822, DateTimeKind.Local).AddTicks(826), 9, new DateTime(2021, 6, 15, 22, 16, 3, 822, DateTimeKind.Local).AddTicks(831) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 20, "Quis voluptatem qui nemo laborum.", new DateTime(2021, 6, 15, 22, 16, 3, 822, DateTimeKind.Local).AddTicks(975), 12, new DateTime(2021, 6, 15, 22, 16, 3, 822, DateTimeKind.Local).AddTicks(980) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "Aut laborum minus aut et magnam quod aliquid ut laboriosam.", new DateTime(2021, 6, 15, 22, 16, 3, 822, DateTimeKind.Local).AddTicks(1051), 1, new DateTime(2021, 6, 15, 22, 16, 3, 822, DateTimeKind.Local).AddTicks(1056) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Ratione rem non.", new DateTime(2021, 6, 15, 22, 16, 3, 822, DateTimeKind.Local).AddTicks(1098), 10, new DateTime(2021, 6, 15, 22, 16, 3, 822, DateTimeKind.Local).AddTicks(1103) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 12, "Non quas doloribus accusamus qui earum.", new DateTime(2021, 6, 15, 22, 16, 3, 822, DateTimeKind.Local).AddTicks(1154), 5, new DateTime(2021, 6, 15, 22, 16, 3, 822, DateTimeKind.Local).AddTicks(1158) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { "Tempora adipisci pariatur animi est eaque ad dolor aliquam doloribus.", new DateTime(2021, 6, 15, 22, 16, 3, 822, DateTimeKind.Local).AddTicks(1330), 19, new DateTime(2021, 6, 15, 22, 16, 3, 822, DateTimeKind.Local).AddTicks(1335) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 16, 3, 540, DateTimeKind.Local).AddTicks(9246), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/793.jpg", new DateTime(2021, 6, 15, 22, 16, 3, 541, DateTimeKind.Local).AddTicks(3120) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 16, 3, 541, DateTimeKind.Local).AddTicks(3718), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/386.jpg", new DateTime(2021, 6, 15, 22, 16, 3, 541, DateTimeKind.Local).AddTicks(3727) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 16, 3, 541, DateTimeKind.Local).AddTicks(3756), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/726.jpg", new DateTime(2021, 6, 15, 22, 16, 3, 541, DateTimeKind.Local).AddTicks(3760) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 16, 3, 541, DateTimeKind.Local).AddTicks(3777), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/543.jpg", new DateTime(2021, 6, 15, 22, 16, 3, 541, DateTimeKind.Local).AddTicks(3781) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 16, 3, 541, DateTimeKind.Local).AddTicks(3796), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/381.jpg", new DateTime(2021, 6, 15, 22, 16, 3, 541, DateTimeKind.Local).AddTicks(3800) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 16, 3, 541, DateTimeKind.Local).AddTicks(3823), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/740.jpg", new DateTime(2021, 6, 15, 22, 16, 3, 541, DateTimeKind.Local).AddTicks(3827) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 16, 3, 541, DateTimeKind.Local).AddTicks(3843), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/350.jpg", new DateTime(2021, 6, 15, 22, 16, 3, 541, DateTimeKind.Local).AddTicks(3847) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 16, 3, 541, DateTimeKind.Local).AddTicks(3862), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/278.jpg", new DateTime(2021, 6, 15, 22, 16, 3, 541, DateTimeKind.Local).AddTicks(3865) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 16, 3, 541, DateTimeKind.Local).AddTicks(3880), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/219.jpg", new DateTime(2021, 6, 15, 22, 16, 3, 541, DateTimeKind.Local).AddTicks(3884) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 16, 3, 541, DateTimeKind.Local).AddTicks(3898), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/8.jpg", new DateTime(2021, 6, 15, 22, 16, 3, 541, DateTimeKind.Local).AddTicks(3901) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 16, 3, 541, DateTimeKind.Local).AddTicks(3917), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/936.jpg", new DateTime(2021, 6, 15, 22, 16, 3, 541, DateTimeKind.Local).AddTicks(3921) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 16, 3, 541, DateTimeKind.Local).AddTicks(3936), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1232.jpg", new DateTime(2021, 6, 15, 22, 16, 3, 541, DateTimeKind.Local).AddTicks(3939) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 16, 3, 541, DateTimeKind.Local).AddTicks(3954), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/54.jpg", new DateTime(2021, 6, 15, 22, 16, 3, 541, DateTimeKind.Local).AddTicks(3958) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 16, 3, 541, DateTimeKind.Local).AddTicks(3985), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/961.jpg", new DateTime(2021, 6, 15, 22, 16, 3, 541, DateTimeKind.Local).AddTicks(3989) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 16, 3, 541, DateTimeKind.Local).AddTicks(4005), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/483.jpg", new DateTime(2021, 6, 15, 22, 16, 3, 541, DateTimeKind.Local).AddTicks(4008) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 16, 3, 541, DateTimeKind.Local).AddTicks(4031), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/987.jpg", new DateTime(2021, 6, 15, 22, 16, 3, 541, DateTimeKind.Local).AddTicks(4035) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 16, 3, 541, DateTimeKind.Local).AddTicks(4050), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/460.jpg", new DateTime(2021, 6, 15, 22, 16, 3, 541, DateTimeKind.Local).AddTicks(4053) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 16, 3, 541, DateTimeKind.Local).AddTicks(4068), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/912.jpg", new DateTime(2021, 6, 15, 22, 16, 3, 541, DateTimeKind.Local).AddTicks(4072) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 16, 3, 541, DateTimeKind.Local).AddTicks(4086), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/620.jpg", new DateTime(2021, 6, 15, 22, 16, 3, 541, DateTimeKind.Local).AddTicks(4090) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 16, 3, 541, DateTimeKind.Local).AddTicks(4104), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1210.jpg", new DateTime(2021, 6, 15, 22, 16, 3, 541, DateTimeKind.Local).AddTicks(4108) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 16, 3, 545, DateTimeKind.Local).AddTicks(179), "https://picsum.photos/640/480/?image=159", new DateTime(2021, 6, 15, 22, 16, 3, 545, DateTimeKind.Local).AddTicks(743) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 16, 3, 545, DateTimeKind.Local).AddTicks(952), "https://picsum.photos/640/480/?image=526", new DateTime(2021, 6, 15, 22, 16, 3, 545, DateTimeKind.Local).AddTicks(958) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 16, 3, 545, DateTimeKind.Local).AddTicks(989), "https://picsum.photos/640/480/?image=437", new DateTime(2021, 6, 15, 22, 16, 3, 545, DateTimeKind.Local).AddTicks(994) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 16, 3, 545, DateTimeKind.Local).AddTicks(1024), "https://picsum.photos/640/480/?image=821", new DateTime(2021, 6, 15, 22, 16, 3, 545, DateTimeKind.Local).AddTicks(1029) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 16, 3, 545, DateTimeKind.Local).AddTicks(1048), "https://picsum.photos/640/480/?image=963", new DateTime(2021, 6, 15, 22, 16, 3, 545, DateTimeKind.Local).AddTicks(1052) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 16, 3, 545, DateTimeKind.Local).AddTicks(1071), "https://picsum.photos/640/480/?image=1033", new DateTime(2021, 6, 15, 22, 16, 3, 545, DateTimeKind.Local).AddTicks(1075) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 16, 3, 545, DateTimeKind.Local).AddTicks(1093), "https://picsum.photos/640/480/?image=189", new DateTime(2021, 6, 15, 22, 16, 3, 545, DateTimeKind.Local).AddTicks(1097) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 16, 3, 545, DateTimeKind.Local).AddTicks(1115), "https://picsum.photos/640/480/?image=738", new DateTime(2021, 6, 15, 22, 16, 3, 545, DateTimeKind.Local).AddTicks(1119) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 16, 3, 545, DateTimeKind.Local).AddTicks(1137), "https://picsum.photos/640/480/?image=412", new DateTime(2021, 6, 15, 22, 16, 3, 545, DateTimeKind.Local).AddTicks(1142) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 16, 3, 545, DateTimeKind.Local).AddTicks(1159), "https://picsum.photos/640/480/?image=590", new DateTime(2021, 6, 15, 22, 16, 3, 545, DateTimeKind.Local).AddTicks(1163) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 16, 3, 545, DateTimeKind.Local).AddTicks(1182), "https://picsum.photos/640/480/?image=111", new DateTime(2021, 6, 15, 22, 16, 3, 545, DateTimeKind.Local).AddTicks(1186) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 16, 3, 545, DateTimeKind.Local).AddTicks(1203), "https://picsum.photos/640/480/?image=634", new DateTime(2021, 6, 15, 22, 16, 3, 545, DateTimeKind.Local).AddTicks(1216) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 16, 3, 545, DateTimeKind.Local).AddTicks(1235), "https://picsum.photos/640/480/?image=79", new DateTime(2021, 6, 15, 22, 16, 3, 545, DateTimeKind.Local).AddTicks(1239) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 16, 3, 545, DateTimeKind.Local).AddTicks(1257), "https://picsum.photos/640/480/?image=852", new DateTime(2021, 6, 15, 22, 16, 3, 545, DateTimeKind.Local).AddTicks(1261) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 16, 3, 545, DateTimeKind.Local).AddTicks(1278), "https://picsum.photos/640/480/?image=70", new DateTime(2021, 6, 15, 22, 16, 3, 545, DateTimeKind.Local).AddTicks(1282) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 16, 3, 545, DateTimeKind.Local).AddTicks(1300), "https://picsum.photos/640/480/?image=109", new DateTime(2021, 6, 15, 22, 16, 3, 545, DateTimeKind.Local).AddTicks(1304) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 16, 3, 545, DateTimeKind.Local).AddTicks(1322), "https://picsum.photos/640/480/?image=841", new DateTime(2021, 6, 15, 22, 16, 3, 545, DateTimeKind.Local).AddTicks(1326) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 16, 3, 545, DateTimeKind.Local).AddTicks(1344), "https://picsum.photos/640/480/?image=714", new DateTime(2021, 6, 15, 22, 16, 3, 545, DateTimeKind.Local).AddTicks(1348) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 16, 3, 545, DateTimeKind.Local).AddTicks(1365), "https://picsum.photos/640/480/?image=319", new DateTime(2021, 6, 15, 22, 16, 3, 545, DateTimeKind.Local).AddTicks(1369) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 16, 3, 545, DateTimeKind.Local).AddTicks(1387), "https://picsum.photos/640/480/?image=118", new DateTime(2021, 6, 15, 22, 16, 3, 545, DateTimeKind.Local).AddTicks(1391) });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "PostId", "ReactionMark", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 20, new DateTime(2021, 6, 15, 22, 16, 3, 829, DateTimeKind.Local).AddTicks(5410), 1, 1, new DateTime(2021, 6, 15, 22, 16, 3, 829, DateTimeKind.Local).AddTicks(5414), 7 },
                    { 1, new DateTime(2021, 6, 15, 22, 16, 3, 829, DateTimeKind.Local).AddTicks(1207), 4, 2, new DateTime(2021, 6, 15, 22, 16, 3, 829, DateTimeKind.Local).AddTicks(1755), 9 },
                    { 19, new DateTime(2021, 6, 15, 22, 16, 3, 829, DateTimeKind.Local).AddTicks(5382), 6, 2, new DateTime(2021, 6, 15, 22, 16, 3, 829, DateTimeKind.Local).AddTicks(5386), 2 },
                    { 18, new DateTime(2021, 6, 15, 22, 16, 3, 829, DateTimeKind.Local).AddTicks(5353), 19, 1, new DateTime(2021, 6, 15, 22, 16, 3, 829, DateTimeKind.Local).AddTicks(5357), 20 }
                });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "PostId", "ReactionMark", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 17, new DateTime(2021, 6, 15, 22, 16, 3, 829, DateTimeKind.Local).AddTicks(5324), 1, 1, new DateTime(2021, 6, 15, 22, 16, 3, 829, DateTimeKind.Local).AddTicks(5329), 4 },
                    { 16, new DateTime(2021, 6, 15, 22, 16, 3, 829, DateTimeKind.Local).AddTicks(5294), 2, 2, new DateTime(2021, 6, 15, 22, 16, 3, 829, DateTimeKind.Local).AddTicks(5299), 3 },
                    { 14, new DateTime(2021, 6, 15, 22, 16, 3, 829, DateTimeKind.Local).AddTicks(5119), 3, 1, new DateTime(2021, 6, 15, 22, 16, 3, 829, DateTimeKind.Local).AddTicks(5123), 7 },
                    { 13, new DateTime(2021, 6, 15, 22, 16, 3, 829, DateTimeKind.Local).AddTicks(5090), 16, 2, new DateTime(2021, 6, 15, 22, 16, 3, 829, DateTimeKind.Local).AddTicks(5094), 2 },
                    { 12, new DateTime(2021, 6, 15, 22, 16, 3, 829, DateTimeKind.Local).AddTicks(5062), 3, 1, new DateTime(2021, 6, 15, 22, 16, 3, 829, DateTimeKind.Local).AddTicks(5066), 21 },
                    { 11, new DateTime(2021, 6, 15, 22, 16, 3, 829, DateTimeKind.Local).AddTicks(5033), 10, 2, new DateTime(2021, 6, 15, 22, 16, 3, 829, DateTimeKind.Local).AddTicks(5037), 11 },
                    { 15, new DateTime(2021, 6, 15, 22, 16, 3, 829, DateTimeKind.Local).AddTicks(5247), 8, 0, new DateTime(2021, 6, 15, 22, 16, 3, 829, DateTimeKind.Local).AddTicks(5254), 7 },
                    { 9, new DateTime(2021, 6, 15, 22, 16, 3, 829, DateTimeKind.Local).AddTicks(4973), 6, 1, new DateTime(2021, 6, 15, 22, 16, 3, 829, DateTimeKind.Local).AddTicks(4977), 1 },
                    { 8, new DateTime(2021, 6, 15, 22, 16, 3, 829, DateTimeKind.Local).AddTicks(4944), 16, 2, new DateTime(2021, 6, 15, 22, 16, 3, 829, DateTimeKind.Local).AddTicks(4948), 3 },
                    { 7, new DateTime(2021, 6, 15, 22, 16, 3, 829, DateTimeKind.Local).AddTicks(4915), 1, 0, new DateTime(2021, 6, 15, 22, 16, 3, 829, DateTimeKind.Local).AddTicks(4920), 2 },
                    { 6, new DateTime(2021, 6, 15, 22, 16, 3, 829, DateTimeKind.Local).AddTicks(4885), 19, 0, new DateTime(2021, 6, 15, 22, 16, 3, 829, DateTimeKind.Local).AddTicks(4889), 16 },
                    { 10, new DateTime(2021, 6, 15, 22, 16, 3, 829, DateTimeKind.Local).AddTicks(5003), 14, 0, new DateTime(2021, 6, 15, 22, 16, 3, 829, DateTimeKind.Local).AddTicks(5007), 19 },
                    { 5, new DateTime(2021, 6, 15, 22, 16, 3, 829, DateTimeKind.Local).AddTicks(4854), 8, 1, new DateTime(2021, 6, 15, 22, 16, 3, 829, DateTimeKind.Local).AddTicks(4858), 14 },
                    { 4, new DateTime(2021, 6, 15, 22, 16, 3, 829, DateTimeKind.Local).AddTicks(4822), 3, 2, new DateTime(2021, 6, 15, 22, 16, 3, 829, DateTimeKind.Local).AddTicks(4826), 18 },
                    { 3, new DateTime(2021, 6, 15, 22, 16, 3, 829, DateTimeKind.Local).AddTicks(4789), 17, 0, new DateTime(2021, 6, 15, 22, 16, 3, 829, DateTimeKind.Local).AddTicks(4793), 3 },
                    { 2, new DateTime(2021, 6, 15, 22, 16, 3, 829, DateTimeKind.Local).AddTicks(4733), 9, 2, new DateTime(2021, 6, 15, 22, 16, 3, 829, DateTimeKind.Local).AddTicks(4742), 1 }
                });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "Aut possimus rem illum. Mollitia hic omnis maxime quo odio. Laboriosam sint eligendi non. Dignissimos doloribus quas cumque unde voluptas.", new DateTime(2021, 6, 15, 22, 16, 3, 816, DateTimeKind.Local).AddTicks(5908), 26, new DateTime(2021, 6, 15, 22, 16, 3, 816, DateTimeKind.Local).AddTicks(6491) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 12, "Fugit minima repudiandae qui autem accusamus eius quod nobis. Aut non rem non vitae eaque voluptatibus dolore non aut. Enim ipsam et repellendus est.", new DateTime(2021, 6, 15, 22, 16, 3, 816, DateTimeKind.Local).AddTicks(7290), 31, new DateTime(2021, 6, 15, 22, 16, 3, 816, DateTimeKind.Local).AddTicks(7300) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 10, "Accusamus cupiditate sint adipisci.", new DateTime(2021, 6, 15, 22, 16, 3, 816, DateTimeKind.Local).AddTicks(7994), 33, new DateTime(2021, 6, 15, 22, 16, 3, 816, DateTimeKind.Local).AddTicks(8010) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "Rem cumque tenetur voluptas ducimus. Laudantium doloremque corrupti laudantium quis. Cumque itaque sed iusto voluptas. Nulla earum voluptatem ea expedita enim voluptatum vitae in perferendis. Error dolorum sed enim rerum.", new DateTime(2021, 6, 15, 22, 16, 3, 816, DateTimeKind.Local).AddTicks(8219), 30, new DateTime(2021, 6, 15, 22, 16, 3, 816, DateTimeKind.Local).AddTicks(8225) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 20, "Et est quia quod voluptatem aspernatur.", new DateTime(2021, 6, 15, 22, 16, 3, 816, DateTimeKind.Local).AddTicks(8284), 23, new DateTime(2021, 6, 15, 22, 16, 3, 816, DateTimeKind.Local).AddTicks(8289) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, "officia", new DateTime(2021, 6, 15, 22, 16, 3, 816, DateTimeKind.Local).AddTicks(8647), 27, new DateTime(2021, 6, 15, 22, 16, 3, 816, DateTimeKind.Local).AddTicks(8660) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, "Assumenda laboriosam fugit. Rerum consequatur commodi. Qui facere molestiae voluptas ipsum aut illum voluptatum accusantium. Velit aspernatur sed.", new DateTime(2021, 6, 15, 22, 16, 3, 816, DateTimeKind.Local).AddTicks(8806), 33, new DateTime(2021, 6, 15, 22, 16, 3, 816, DateTimeKind.Local).AddTicks(8811) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, "Ipsum unde sint doloribus cumque occaecati possimus.", new DateTime(2021, 6, 15, 22, 16, 3, 816, DateTimeKind.Local).AddTicks(8910), 23, new DateTime(2021, 6, 15, 22, 16, 3, 816, DateTimeKind.Local).AddTicks(8916) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 3, "Dolores est veritatis et sapiente.\nDoloribus explicabo aliquam ut sint aliquid aut quis est.\nIn tenetur voluptatem illo omnis corrupti.\nQui ducimus est placeat incidunt ipsum.", new DateTime(2021, 6, 15, 22, 16, 3, 816, DateTimeKind.Local).AddTicks(9588), 35, new DateTime(2021, 6, 15, 22, 16, 3, 816, DateTimeKind.Local).AddTicks(9605) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, "Necessitatibus nisi ut mollitia.\nPlaceat nostrum labore corporis.\nEt similique debitis esse.", new DateTime(2021, 6, 15, 22, 16, 3, 816, DateTimeKind.Local).AddTicks(9720), 37, new DateTime(2021, 6, 15, 22, 16, 3, 816, DateTimeKind.Local).AddTicks(9726) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { "Quos similique facilis aliquam et.\nAmet quia possimus.", new DateTime(2021, 6, 15, 22, 16, 3, 816, DateTimeKind.Local).AddTicks(9794), new DateTime(2021, 6, 15, 22, 16, 3, 816, DateTimeKind.Local).AddTicks(9799) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, "Voluptatibus qui ut. Aut quibusdam ea. Ut amet enim adipisci aut eveniet suscipit in consequatur. Aperiam nesciunt est natus et sunt odit perspiciatis ut et.", new DateTime(2021, 6, 15, 22, 16, 3, 816, DateTimeKind.Local).AddTicks(9939), 31, new DateTime(2021, 6, 15, 22, 16, 3, 816, DateTimeKind.Local).AddTicks(9945) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 9, "Atque eveniet sit atque. Sint exercitationem hic qui doloremque. Voluptatibus ab quia nostrum repellendus ea. Ut vel magnam.", new DateTime(2021, 6, 15, 22, 16, 3, 817, DateTimeKind.Local).AddTicks(52), 27, new DateTime(2021, 6, 15, 22, 16, 3, 817, DateTimeKind.Local).AddTicks(57) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, "aliquam", new DateTime(2021, 6, 15, 22, 16, 3, 817, DateTimeKind.Local).AddTicks(86), 39, new DateTime(2021, 6, 15, 22, 16, 3, 817, DateTimeKind.Local).AddTicks(91) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 10, "Modi quam et aut. Repellat voluptatem repudiandae quos beatae id. Quae non repellendus odit. Consequatur cum voluptas. Velit tempora sed delectus. Voluptatum et incidunt autem.", new DateTime(2021, 6, 15, 22, 16, 3, 817, DateTimeKind.Local).AddTicks(233), 34, new DateTime(2021, 6, 15, 22, 16, 3, 817, DateTimeKind.Local).AddTicks(238) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, "et", new DateTime(2021, 6, 15, 22, 16, 3, 817, DateTimeKind.Local).AddTicks(269), 37, new DateTime(2021, 6, 15, 22, 16, 3, 817, DateTimeKind.Local).AddTicks(274) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, "architecto", new DateTime(2021, 6, 15, 22, 16, 3, 817, DateTimeKind.Local).AddTicks(302), 36, new DateTime(2021, 6, 15, 22, 16, 3, 817, DateTimeKind.Local).AddTicks(307) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "Fuga praesentium beatae ipsa voluptatem necessitatibus.", new DateTime(2021, 6, 15, 22, 16, 3, 817, DateTimeKind.Local).AddTicks(357), 28, new DateTime(2021, 6, 15, 22, 16, 3, 817, DateTimeKind.Local).AddTicks(362) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, "Recusandae illo accusantium consectetur. Aut eius occaecati qui nobis sed. Ut eaque perspiciatis est quod quae. Deserunt eum vel minus sunt voluptas blanditiis et repudiandae officia. Vitae dolores voluptas voluptatem labore ducimus dolor. Soluta veritatis iste est perferendis possimus quas ipsa vel.", new DateTime(2021, 6, 15, 22, 16, 3, 817, DateTimeKind.Local).AddTicks(1564), 21, new DateTime(2021, 6, 15, 22, 16, 3, 817, DateTimeKind.Local).AddTicks(1579) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 19, "fuga", new DateTime(2021, 6, 15, 22, 16, 3, 817, DateTimeKind.Local).AddTicks(1642), 23, new DateTime(2021, 6, 15, 22, 16, 3, 817, DateTimeKind.Local).AddTicks(1647) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2021, 6, 15, 22, 16, 3, 597, DateTimeKind.Local).AddTicks(3377), "Flavie.Raynor67@gmail.com", "m74rX8ycT8o9Ij69B6RnH0iPikhAJNK1/Ndh3c+QCgM=", "TIuZWh7N0Vsf7SRGxxeBqeYWnWmkyHgNsaE0DUmYQ7E=", new DateTime(2021, 6, 15, 22, 16, 3, 597, DateTimeKind.Local).AddTicks(4007), "Abigail.Bergstrom" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2021, 6, 15, 22, 16, 3, 606, DateTimeKind.Local).AddTicks(688), "Gerson89@yahoo.com", "6cILTjGPp8l7CEygSRBpTA7ZMo8YMyuI2wvGT+lgTTs=", "dEfzxQFqU1DcPZnIpQ5O1OMMdqKXFFZGasIUXAwb4eo=", new DateTime(2021, 6, 15, 22, 16, 3, 606, DateTimeKind.Local).AddTicks(743), "Mckenna70" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 16, new DateTime(2021, 6, 15, 22, 16, 3, 614, DateTimeKind.Local).AddTicks(1381), "Fanny.Bernhard@hotmail.com", "u1ffttZpAC8PYjtEeBEDFgU+Zgw4ls1zMGMYuCF5WHM=", "r6U2q1zQnMnBordYPS6uXI3ScSW/Ev529TfjME2DVvI=", new DateTime(2021, 6, 15, 22, 16, 3, 614, DateTimeKind.Local).AddTicks(1422), "Bertha.Weissnat22" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2021, 6, 15, 22, 16, 3, 644, DateTimeKind.Local).AddTicks(3182), "Maureen_Macejkovic80@yahoo.com", "UdKN+ArAlSwvEXoaornN+UHq8FkQEfOdYk+hI9DcemM=", "FvgDVh8RpgJK/gICvop2HRHAIiU/fywsuqGqXBpSxZw=", new DateTime(2021, 6, 15, 22, 16, 3, 644, DateTimeKind.Local).AddTicks(3269), "Pascale59" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 16, new DateTime(2021, 6, 15, 22, 16, 3, 679, DateTimeKind.Local).AddTicks(1570), "Orlando.Cremin@hotmail.com", "BY8heotq/IUd6+mtzZYu+PH5O2boYD/AvtaXxbzlltk=", "OVqErDDWjNzwncGsE+R3MC5wCyyb31g1+zk909kZTJM=", new DateTime(2021, 6, 15, 22, 16, 3, 679, DateTimeKind.Local).AddTicks(1642), "Delbert.Bergnaum" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2021, 6, 15, 22, 16, 3, 690, DateTimeKind.Local).AddTicks(7872), "Al_Kertzmann47@gmail.com", "VcWKIe2lG/NTnDFHsVtXhZRXsqOMEQXNqpl2qvw7lco=", "s/JOaOoLX+XaQWOVEI/E5bSPTgm8W9j93IorgDkBNMo=", new DateTime(2021, 6, 15, 22, 16, 3, 690, DateTimeKind.Local).AddTicks(7946), "Lionel.DAmore8" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2021, 6, 15, 22, 16, 3, 699, DateTimeKind.Local).AddTicks(2531), "Orin.Armstrong42@hotmail.com", "SXdwAWlXUJbvFbHH2DhflCJfwqw3LJUcE6dnfHM7qx4=", "ws6kF0tlsoMw5SGTLVwQ38QTmI0JwE+Y2Ah9A0+1vNc=", new DateTime(2021, 6, 15, 22, 16, 3, 699, DateTimeKind.Local).AddTicks(2597), "Nels_Haley59" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 12, new DateTime(2021, 6, 15, 22, 16, 3, 707, DateTimeKind.Local).AddTicks(7532), "Caitlyn_Jast31@gmail.com", "z6+lMGxBFevByq0tVJ4jFUCG3XV4XnSN+oMcPAGSpec=", "/VfwYpg4Ld8uieJ84pPYfQBW6JZVYz8d341z6dK9ykc=", new DateTime(2021, 6, 15, 22, 16, 3, 707, DateTimeKind.Local).AddTicks(7595), "Giovanni.Bergnaum60" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2021, 6, 15, 22, 16, 3, 715, DateTimeKind.Local).AddTicks(5323), "Daren.Muller39@hotmail.com", "3EHSFADwHPrKte46kROS6oxbo8elhsn2lb+UYqzUiIY=", "u2aPZP/GXlfOOwGBAcUMNRRRYB4AqrtjwnhYoyqLGPM=", new DateTime(2021, 6, 15, 22, 16, 3, 715, DateTimeKind.Local).AddTicks(5351), "Herman_Casper80" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2021, 6, 15, 22, 16, 3, 723, DateTimeKind.Local).AddTicks(3392), "Eden_Rohan78@hotmail.com", "5x2o9kyE2GmUGEguzo8od0TSVVGZ9kX6dgK0d0ReZ4Q=", "c6YDqydBBDDOv7PHPkkeH8ctTQdRqcMTkDS5qjqi85c=", new DateTime(2021, 6, 15, 22, 16, 3, 723, DateTimeKind.Local).AddTicks(3422), "Elliott_Pfannerstill" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2021, 6, 15, 22, 16, 3, 731, DateTimeKind.Local).AddTicks(879), "Van70@gmail.com", "Uk2lasTpXoKtOBHfboeOqzUBf5PwYXnHrIhwwfMkfpk=", "xdsEUxoLf1gSCzGRqFMhyItBOHfZWiGVagL8BBhNDxQ=", new DateTime(2021, 6, 15, 22, 16, 3, 731, DateTimeKind.Local).AddTicks(908), "Olen28" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2021, 6, 15, 22, 16, 3, 738, DateTimeKind.Local).AddTicks(7976), "Eugene.Dibbert@hotmail.com", "JNRrmyqRFHsyf3SY9GWSbOhqXPcXpu80TeJ7DU+0kXg=", "x6ZuQBQzYxJ1LR+5NC5oBeW5G4tAllodCH1RA/73B/c=", new DateTime(2021, 6, 15, 22, 16, 3, 738, DateTimeKind.Local).AddTicks(8001), "Isidro.Littel12" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 16, 3, 746, DateTimeKind.Local).AddTicks(5022), "Sienna.Larson@hotmail.com", "1ar4pEZGPiaBEBPvJ4nn4/MvHvehCx7eMioE6eS9xLg=", "UVbaB7WpoX3vecubmPNQJUWaE8KByiy6BDne1gpdRlg=", new DateTime(2021, 6, 15, 22, 16, 3, 746, DateTimeKind.Local).AddTicks(5051), "Verona25" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2021, 6, 15, 22, 16, 3, 754, DateTimeKind.Local).AddTicks(2711), "Aliza16@gmail.com", "th/xhwbuPt6TwzInQM1hXnwcETFJ6YjD0262qzlMEy0=", "+tWSuDuJuyxcTZYNq+R46xJu63IYi5uY4HGmBgmyBpk=", new DateTime(2021, 6, 15, 22, 16, 3, 754, DateTimeKind.Local).AddTicks(2730), "Kaden_Powlowski6" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 16, 3, 762, DateTimeKind.Local).AddTicks(1753), "Mitchell60@yahoo.com", "SksDim41oRl46E2AVhNO16s7zCCryGEhOrvUDt8l2M8=", "0oJgfdAnu2FSbmS7fW6i7pSoMbMhaU8jIyrGu4AJgos=", new DateTime(2021, 6, 15, 22, 16, 3, 762, DateTimeKind.Local).AddTicks(1784), "Marco_Kessler" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2021, 6, 15, 22, 16, 3, 769, DateTimeKind.Local).AddTicks(4261), "Carmella_Hilll2@yahoo.com", "AQGAAUj6Cg26rm+dob6SwkLMdTqwrW4wWUWAYgo1Y8g=", "fEujCmorn5UDNrjQjnzEEzCLF+4BJw5Ewh9MWHvT4ho=", new DateTime(2021, 6, 15, 22, 16, 3, 769, DateTimeKind.Local).AddTicks(4277), "Alexandro39" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2021, 6, 15, 22, 16, 3, 777, DateTimeKind.Local).AddTicks(2710), "Ruben.Kulas61@gmail.com", "Qo3Negz/J21mS1E78ucYpDjs9uLRHLMcsVVpA6Yj8zA=", "ME7bnpgdUoKe80K8fVZNR099dyPejWF7pZJWOv5wlx4=", new DateTime(2021, 6, 15, 22, 16, 3, 777, DateTimeKind.Local).AddTicks(2737), "Chanelle.Goyette7" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 5, new DateTime(2021, 6, 15, 22, 16, 3, 784, DateTimeKind.Local).AddTicks(7976), "Scarlett.Thompson@yahoo.com", "Fr9CtnQXREyHGkjuoi8BErR/Cr9nl/VC7XBd12+aTR8=", "2hphnu1ljh6qp1fdqSy6YIKzmlkzUn168/z0O7AKCfc=", new DateTime(2021, 6, 15, 22, 16, 3, 784, DateTimeKind.Local).AddTicks(7992), "Amie_Witting97" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2021, 6, 15, 22, 16, 3, 792, DateTimeKind.Local).AddTicks(5457), "Javon_Raynor26@yahoo.com", "YPtqlXm1upLomypYjLJleHRBwBgCFnjV/UGtUdjm5C8=", "0aCLNtZZd44iJx3kYWfT6YGqeNTJ6kJTeX18NKse6p0=", new DateTime(2021, 6, 15, 22, 16, 3, 792, DateTimeKind.Local).AddTicks(5486), "Shanel.Kuhic16" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2021, 6, 15, 22, 16, 3, 799, DateTimeKind.Local).AddTicks(9716), "Joanny32@yahoo.com", "ANO0vTLaXAsCaUXe5ed9+qLnZDwC+FRImIsvMiJdKFc=", "jZUNTT+Z0Uare5O3UIjxBZyC+GVI4EfxmxycM0v70/A=", new DateTime(2021, 6, 15, 22, 16, 3, 799, DateTimeKind.Local).AddTicks(9732), "Brooke_Thompson99" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 22, 16, 3, 807, DateTimeKind.Local).AddTicks(5263), "KLDMlqvgd19nG7dqyPT082zpLn1TQKdu4iaTszcF+Sc=", "c+rc0c34+eYa+/TDtAewHtNMnjhITCD5k4jtVlfo2M4=", new DateTime(2021, 6, 15, 22, 16, 3, 807, DateTimeKind.Local).AddTicks(5263) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DropColumn(
                name: "ReactionMark",
                table: "PostReactions");

            migrationBuilder.DropColumn(
                name: "ReactionMark",
                table: "CommentReactions");

            migrationBuilder.AddColumn<bool>(
                name: "IsLike",
                table: "PostReactions",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsLike",
                table: "CommentReactions",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.InsertData(
                table: "CommentReactions",
                columns: new[] { "Id", "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 1, 10, new DateTime(2021, 6, 15, 0, 5, 52, 821, DateTimeKind.Local).AddTicks(9384), false, new DateTime(2021, 6, 15, 0, 5, 52, 821, DateTimeKind.Local).AddTicks(9924), 10 },
                    { 18, 14, new DateTime(2021, 6, 15, 0, 5, 52, 822, DateTimeKind.Local).AddTicks(748), false, new DateTime(2021, 6, 15, 0, 5, 52, 822, DateTimeKind.Local).AddTicks(751), 14 },
                    { 17, 5, new DateTime(2021, 6, 15, 0, 5, 52, 822, DateTimeKind.Local).AddTicks(725), false, new DateTime(2021, 6, 15, 0, 5, 52, 822, DateTimeKind.Local).AddTicks(729), 6 },
                    { 15, 18, new DateTime(2021, 6, 15, 0, 5, 52, 822, DateTimeKind.Local).AddTicks(681), true, new DateTime(2021, 6, 15, 0, 5, 52, 822, DateTimeKind.Local).AddTicks(685), 18 },
                    { 14, 15, new DateTime(2021, 6, 15, 0, 5, 52, 822, DateTimeKind.Local).AddTicks(660), false, new DateTime(2021, 6, 15, 0, 5, 52, 822, DateTimeKind.Local).AddTicks(664), 21 },
                    { 13, 5, new DateTime(2021, 6, 15, 0, 5, 52, 822, DateTimeKind.Local).AddTicks(638), true, new DateTime(2021, 6, 15, 0, 5, 52, 822, DateTimeKind.Local).AddTicks(642), 16 },
                    { 12, 7, new DateTime(2021, 6, 15, 0, 5, 52, 822, DateTimeKind.Local).AddTicks(616), false, new DateTime(2021, 6, 15, 0, 5, 52, 822, DateTimeKind.Local).AddTicks(620), 12 },
                    { 11, 17, new DateTime(2021, 6, 15, 0, 5, 52, 822, DateTimeKind.Local).AddTicks(587), true, new DateTime(2021, 6, 15, 0, 5, 52, 822, DateTimeKind.Local).AddTicks(591), 17 },
                    { 19, 9, new DateTime(2021, 6, 15, 0, 5, 52, 822, DateTimeKind.Local).AddTicks(769), true, new DateTime(2021, 6, 15, 0, 5, 52, 822, DateTimeKind.Local).AddTicks(773), 13 },
                    { 10, 17, new DateTime(2021, 6, 15, 0, 5, 52, 822, DateTimeKind.Local).AddTicks(566), false, new DateTime(2021, 6, 15, 0, 5, 52, 822, DateTimeKind.Local).AddTicks(570), 3 },
                    { 8, 9, new DateTime(2021, 6, 15, 0, 5, 52, 822, DateTimeKind.Local).AddTicks(524), true, new DateTime(2021, 6, 15, 0, 5, 52, 822, DateTimeKind.Local).AddTicks(528), 1 },
                    { 7, 6, new DateTime(2021, 6, 15, 0, 5, 52, 822, DateTimeKind.Local).AddTicks(502), false, new DateTime(2021, 6, 15, 0, 5, 52, 822, DateTimeKind.Local).AddTicks(506), 17 },
                    { 6, 17, new DateTime(2021, 6, 15, 0, 5, 52, 822, DateTimeKind.Local).AddTicks(480), true, new DateTime(2021, 6, 15, 0, 5, 52, 822, DateTimeKind.Local).AddTicks(484), 1 },
                    { 5, 3, new DateTime(2021, 6, 15, 0, 5, 52, 822, DateTimeKind.Local).AddTicks(458), false, new DateTime(2021, 6, 15, 0, 5, 52, 822, DateTimeKind.Local).AddTicks(462), 18 },
                    { 4, 12, new DateTime(2021, 6, 15, 0, 5, 52, 822, DateTimeKind.Local).AddTicks(435), false, new DateTime(2021, 6, 15, 0, 5, 52, 822, DateTimeKind.Local).AddTicks(439), 19 },
                    { 3, 8, new DateTime(2021, 6, 15, 0, 5, 52, 822, DateTimeKind.Local).AddTicks(411), true, new DateTime(2021, 6, 15, 0, 5, 52, 822, DateTimeKind.Local).AddTicks(415), 1 },
                    { 2, 1, new DateTime(2021, 6, 15, 0, 5, 52, 822, DateTimeKind.Local).AddTicks(362), false, new DateTime(2021, 6, 15, 0, 5, 52, 822, DateTimeKind.Local).AddTicks(370), 19 },
                    { 9, 9, new DateTime(2021, 6, 15, 0, 5, 52, 822, DateTimeKind.Local).AddTicks(544), false, new DateTime(2021, 6, 15, 0, 5, 52, 822, DateTimeKind.Local).AddTicks(548), 17 },
                    { 20, 2, new DateTime(2021, 6, 15, 0, 5, 52, 822, DateTimeKind.Local).AddTicks(790), true, new DateTime(2021, 6, 15, 0, 5, 52, 822, DateTimeKind.Local).AddTicks(794), 18 },
                    { 16, 19, new DateTime(2021, 6, 15, 0, 5, 52, 822, DateTimeKind.Local).AddTicks(704), true, new DateTime(2021, 6, 15, 0, 5, 52, 822, DateTimeKind.Local).AddTicks(708), 7 }
                });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 12, "Corrupti dolor rerum est nihil.", new DateTime(2021, 6, 15, 0, 5, 52, 811, DateTimeKind.Local).AddTicks(6930), 10, new DateTime(2021, 6, 15, 0, 5, 52, 811, DateTimeKind.Local).AddTicks(7444) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "Rem et enim culpa voluptas.", new DateTime(2021, 6, 15, 0, 5, 52, 811, DateTimeKind.Local).AddTicks(7919), 12, new DateTime(2021, 6, 15, 0, 5, 52, 811, DateTimeKind.Local).AddTicks(7927) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 15, "Illo ut reprehenderit ut.", new DateTime(2021, 6, 15, 0, 5, 52, 811, DateTimeKind.Local).AddTicks(7990), 5, new DateTime(2021, 6, 15, 0, 5, 52, 811, DateTimeKind.Local).AddTicks(7995) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Aut dolorem rem voluptatum.", new DateTime(2021, 6, 15, 0, 5, 52, 811, DateTimeKind.Local).AddTicks(8039), 11, new DateTime(2021, 6, 15, 0, 5, 52, 811, DateTimeKind.Local).AddTicks(8043) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 11, "Unde dolore error inventore et.", new DateTime(2021, 6, 15, 0, 5, 52, 811, DateTimeKind.Local).AddTicks(8102), 1, new DateTime(2021, 6, 15, 0, 5, 52, 811, DateTimeKind.Local).AddTicks(8106) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 12, "Aliquam repudiandae et placeat provident et odit cupiditate aperiam.", new DateTime(2021, 6, 15, 0, 5, 52, 811, DateTimeKind.Local).AddTicks(8168), 9, new DateTime(2021, 6, 15, 0, 5, 52, 811, DateTimeKind.Local).AddTicks(8172) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { "Neque dolores quaerat sit dolorum.", new DateTime(2021, 6, 15, 0, 5, 52, 811, DateTimeKind.Local).AddTicks(8215), 1, new DateTime(2021, 6, 15, 0, 5, 52, 811, DateTimeKind.Local).AddTicks(8219) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 6, "Iste tenetur architecto quas possimus amet occaecati accusantium perferendis.", new DateTime(2021, 6, 15, 0, 5, 52, 811, DateTimeKind.Local).AddTicks(8301), 15, new DateTime(2021, 6, 15, 0, 5, 52, 811, DateTimeKind.Local).AddTicks(8306) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 11, "Sed minima facilis.", new DateTime(2021, 6, 15, 0, 5, 52, 811, DateTimeKind.Local).AddTicks(8346), 2, new DateTime(2021, 6, 15, 0, 5, 52, 811, DateTimeKind.Local).AddTicks(8350) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Est veniam omnis facilis odio repudiandae iste ut sed ab.", new DateTime(2021, 6, 15, 0, 5, 52, 811, DateTimeKind.Local).AddTicks(8405), 8, new DateTime(2021, 6, 15, 0, 5, 52, 811, DateTimeKind.Local).AddTicks(8410) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 9, "Quibusdam ducimus magnam numquam corporis quo.", new DateTime(2021, 6, 15, 0, 5, 52, 811, DateTimeKind.Local).AddTicks(8454), 20, new DateTime(2021, 6, 15, 0, 5, 52, 811, DateTimeKind.Local).AddTicks(8459) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 6, "Ut beatae molestias voluptas dolorem.", new DateTime(2021, 6, 15, 0, 5, 52, 811, DateTimeKind.Local).AddTicks(8498), 4, new DateTime(2021, 6, 15, 0, 5, 52, 811, DateTimeKind.Local).AddTicks(8502) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 20, "At dolor sed nobis in placeat voluptatibus repudiandae sint neque.", new DateTime(2021, 6, 15, 0, 5, 52, 811, DateTimeKind.Local).AddTicks(8557), 11, new DateTime(2021, 6, 15, 0, 5, 52, 811, DateTimeKind.Local).AddTicks(8561) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Totam eaque harum.", new DateTime(2021, 6, 15, 0, 5, 52, 811, DateTimeKind.Local).AddTicks(8595), 19, new DateTime(2021, 6, 15, 0, 5, 52, 811, DateTimeKind.Local).AddTicks(8600) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 15, "Accusamus veritatis nisi rerum nam earum ut recusandae.", new DateTime(2021, 6, 15, 0, 5, 52, 811, DateTimeKind.Local).AddTicks(8653), 3, new DateTime(2021, 6, 15, 0, 5, 52, 811, DateTimeKind.Local).AddTicks(8657) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 5, "Sunt ad ab ut velit quia quaerat facere.", new DateTime(2021, 6, 15, 0, 5, 52, 811, DateTimeKind.Local).AddTicks(8704), 14, new DateTime(2021, 6, 15, 0, 5, 52, 811, DateTimeKind.Local).AddTicks(8709) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 6, "Ullam temporibus cum qui totam.", new DateTime(2021, 6, 15, 0, 5, 52, 811, DateTimeKind.Local).AddTicks(8748), 18, new DateTime(2021, 6, 15, 0, 5, 52, 811, DateTimeKind.Local).AddTicks(8752) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "Officiis itaque aut.", new DateTime(2021, 6, 15, 0, 5, 52, 811, DateTimeKind.Local).AddTicks(8787), 7, new DateTime(2021, 6, 15, 0, 5, 52, 811, DateTimeKind.Local).AddTicks(8791) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 2, "Molestias eos blanditiis quod incidunt illum veniam dolor voluptas.", new DateTime(2021, 6, 15, 0, 5, 52, 811, DateTimeKind.Local).AddTicks(8840), 14, new DateTime(2021, 6, 15, 0, 5, 52, 811, DateTimeKind.Local).AddTicks(8845) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { "Fugiat ut fugiat quis architecto est.", new DateTime(2021, 6, 15, 0, 5, 52, 811, DateTimeKind.Local).AddTicks(8886), 13, new DateTime(2021, 6, 15, 0, 5, 52, 811, DateTimeKind.Local).AddTicks(8890) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 0, 5, 52, 609, DateTimeKind.Local).AddTicks(6908), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1106.jpg", new DateTime(2021, 6, 15, 0, 5, 52, 610, DateTimeKind.Local).AddTicks(590) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 0, 5, 52, 610, DateTimeKind.Local).AddTicks(1157), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1053.jpg", new DateTime(2021, 6, 15, 0, 5, 52, 610, DateTimeKind.Local).AddTicks(1164) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 0, 5, 52, 610, DateTimeKind.Local).AddTicks(1185), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/356.jpg", new DateTime(2021, 6, 15, 0, 5, 52, 610, DateTimeKind.Local).AddTicks(1189) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 0, 5, 52, 610, DateTimeKind.Local).AddTicks(1202), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/140.jpg", new DateTime(2021, 6, 15, 0, 5, 52, 610, DateTimeKind.Local).AddTicks(1206) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 0, 5, 52, 610, DateTimeKind.Local).AddTicks(1220), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/501.jpg", new DateTime(2021, 6, 15, 0, 5, 52, 610, DateTimeKind.Local).AddTicks(1223) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 0, 5, 52, 610, DateTimeKind.Local).AddTicks(1237), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/886.jpg", new DateTime(2021, 6, 15, 0, 5, 52, 610, DateTimeKind.Local).AddTicks(1241) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 0, 5, 52, 610, DateTimeKind.Local).AddTicks(1255), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1140.jpg", new DateTime(2021, 6, 15, 0, 5, 52, 610, DateTimeKind.Local).AddTicks(1258) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 0, 5, 52, 610, DateTimeKind.Local).AddTicks(1271), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/989.jpg", new DateTime(2021, 6, 15, 0, 5, 52, 610, DateTimeKind.Local).AddTicks(1275) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 0, 5, 52, 610, DateTimeKind.Local).AddTicks(1288), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/820.jpg", new DateTime(2021, 6, 15, 0, 5, 52, 610, DateTimeKind.Local).AddTicks(1291) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 0, 5, 52, 610, DateTimeKind.Local).AddTicks(1387), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1204.jpg", new DateTime(2021, 6, 15, 0, 5, 52, 610, DateTimeKind.Local).AddTicks(1392) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 0, 5, 52, 610, DateTimeKind.Local).AddTicks(1408), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1053.jpg", new DateTime(2021, 6, 15, 0, 5, 52, 610, DateTimeKind.Local).AddTicks(1411) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 0, 5, 52, 610, DateTimeKind.Local).AddTicks(1424), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1186.jpg", new DateTime(2021, 6, 15, 0, 5, 52, 610, DateTimeKind.Local).AddTicks(1428) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 0, 5, 52, 610, DateTimeKind.Local).AddTicks(1441), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/274.jpg", new DateTime(2021, 6, 15, 0, 5, 52, 610, DateTimeKind.Local).AddTicks(1445) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 0, 5, 52, 610, DateTimeKind.Local).AddTicks(1458), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/834.jpg", new DateTime(2021, 6, 15, 0, 5, 52, 610, DateTimeKind.Local).AddTicks(1462) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 0, 5, 52, 610, DateTimeKind.Local).AddTicks(1475), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/607.jpg", new DateTime(2021, 6, 15, 0, 5, 52, 610, DateTimeKind.Local).AddTicks(1479) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 0, 5, 52, 610, DateTimeKind.Local).AddTicks(1492), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1179.jpg", new DateTime(2021, 6, 15, 0, 5, 52, 610, DateTimeKind.Local).AddTicks(1496) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 0, 5, 52, 610, DateTimeKind.Local).AddTicks(1509), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/809.jpg", new DateTime(2021, 6, 15, 0, 5, 52, 610, DateTimeKind.Local).AddTicks(1512) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 0, 5, 52, 610, DateTimeKind.Local).AddTicks(1526), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/315.jpg", new DateTime(2021, 6, 15, 0, 5, 52, 610, DateTimeKind.Local).AddTicks(1529) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 0, 5, 52, 610, DateTimeKind.Local).AddTicks(1543), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/427.jpg", new DateTime(2021, 6, 15, 0, 5, 52, 610, DateTimeKind.Local).AddTicks(1546) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 0, 5, 52, 610, DateTimeKind.Local).AddTicks(1559), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/689.jpg", new DateTime(2021, 6, 15, 0, 5, 52, 610, DateTimeKind.Local).AddTicks(1562) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 0, 5, 52, 613, DateTimeKind.Local).AddTicks(9235), "https://picsum.photos/640/480/?image=628", new DateTime(2021, 6, 15, 0, 5, 52, 613, DateTimeKind.Local).AddTicks(9756) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 0, 5, 52, 613, DateTimeKind.Local).AddTicks(9934), "https://picsum.photos/640/480/?image=1080", new DateTime(2021, 6, 15, 0, 5, 52, 613, DateTimeKind.Local).AddTicks(9940) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 0, 5, 52, 613, DateTimeKind.Local).AddTicks(9958), "https://picsum.photos/640/480/?image=783", new DateTime(2021, 6, 15, 0, 5, 52, 613, DateTimeKind.Local).AddTicks(9962) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 0, 5, 52, 614, DateTimeKind.Local).AddTicks(58), "https://picsum.photos/640/480/?image=482", new DateTime(2021, 6, 15, 0, 5, 52, 614, DateTimeKind.Local).AddTicks(63) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 0, 5, 52, 614, DateTimeKind.Local).AddTicks(79), "https://picsum.photos/640/480/?image=515", new DateTime(2021, 6, 15, 0, 5, 52, 614, DateTimeKind.Local).AddTicks(83) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 0, 5, 52, 614, DateTimeKind.Local).AddTicks(97), "https://picsum.photos/640/480/?image=986", new DateTime(2021, 6, 15, 0, 5, 52, 614, DateTimeKind.Local).AddTicks(101) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 0, 5, 52, 614, DateTimeKind.Local).AddTicks(115), "https://picsum.photos/640/480/?image=229", new DateTime(2021, 6, 15, 0, 5, 52, 614, DateTimeKind.Local).AddTicks(119) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 0, 5, 52, 614, DateTimeKind.Local).AddTicks(133), "https://picsum.photos/640/480/?image=937", new DateTime(2021, 6, 15, 0, 5, 52, 614, DateTimeKind.Local).AddTicks(136) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 0, 5, 52, 614, DateTimeKind.Local).AddTicks(151), "https://picsum.photos/640/480/?image=18", new DateTime(2021, 6, 15, 0, 5, 52, 614, DateTimeKind.Local).AddTicks(154) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 0, 5, 52, 614, DateTimeKind.Local).AddTicks(168), "https://picsum.photos/640/480/?image=452", new DateTime(2021, 6, 15, 0, 5, 52, 614, DateTimeKind.Local).AddTicks(171) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 0, 5, 52, 614, DateTimeKind.Local).AddTicks(185), "https://picsum.photos/640/480/?image=192", new DateTime(2021, 6, 15, 0, 5, 52, 614, DateTimeKind.Local).AddTicks(189) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 0, 5, 52, 614, DateTimeKind.Local).AddTicks(203), "https://picsum.photos/640/480/?image=109", new DateTime(2021, 6, 15, 0, 5, 52, 614, DateTimeKind.Local).AddTicks(206) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 0, 5, 52, 614, DateTimeKind.Local).AddTicks(220), "https://picsum.photos/640/480/?image=432", new DateTime(2021, 6, 15, 0, 5, 52, 614, DateTimeKind.Local).AddTicks(223) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 0, 5, 52, 614, DateTimeKind.Local).AddTicks(237), "https://picsum.photos/640/480/?image=533", new DateTime(2021, 6, 15, 0, 5, 52, 614, DateTimeKind.Local).AddTicks(241) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 0, 5, 52, 614, DateTimeKind.Local).AddTicks(255), "https://picsum.photos/640/480/?image=1005", new DateTime(2021, 6, 15, 0, 5, 52, 614, DateTimeKind.Local).AddTicks(258) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 0, 5, 52, 614, DateTimeKind.Local).AddTicks(272), "https://picsum.photos/640/480/?image=1007", new DateTime(2021, 6, 15, 0, 5, 52, 614, DateTimeKind.Local).AddTicks(276) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 0, 5, 52, 614, DateTimeKind.Local).AddTicks(323), "https://picsum.photos/640/480/?image=903", new DateTime(2021, 6, 15, 0, 5, 52, 614, DateTimeKind.Local).AddTicks(328) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 0, 5, 52, 614, DateTimeKind.Local).AddTicks(344), "https://picsum.photos/640/480/?image=1014", new DateTime(2021, 6, 15, 0, 5, 52, 614, DateTimeKind.Local).AddTicks(348) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 0, 5, 52, 614, DateTimeKind.Local).AddTicks(362), "https://picsum.photos/640/480/?image=581", new DateTime(2021, 6, 15, 0, 5, 52, 614, DateTimeKind.Local).AddTicks(365) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 0, 5, 52, 614, DateTimeKind.Local).AddTicks(379), "https://picsum.photos/640/480/?image=857", new DateTime(2021, 6, 15, 0, 5, 52, 614, DateTimeKind.Local).AddTicks(382) });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 20, new DateTime(2021, 6, 15, 0, 5, 52, 817, DateTimeKind.Local).AddTicks(3005), true, 10, new DateTime(2021, 6, 15, 0, 5, 52, 817, DateTimeKind.Local).AddTicks(3009), 10 },
                    { 1, new DateTime(2021, 6, 15, 0, 5, 52, 817, DateTimeKind.Local).AddTicks(923), false, 20, new DateTime(2021, 6, 15, 0, 5, 52, 817, DateTimeKind.Local).AddTicks(1999), 12 },
                    { 19, new DateTime(2021, 6, 15, 0, 5, 52, 817, DateTimeKind.Local).AddTicks(2984), false, 13, new DateTime(2021, 6, 15, 0, 5, 52, 817, DateTimeKind.Local).AddTicks(2987), 6 },
                    { 18, new DateTime(2021, 6, 15, 0, 5, 52, 817, DateTimeKind.Local).AddTicks(2963), true, 2, new DateTime(2021, 6, 15, 0, 5, 52, 817, DateTimeKind.Local).AddTicks(2967), 16 }
                });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 17, new DateTime(2021, 6, 15, 0, 5, 52, 817, DateTimeKind.Local).AddTicks(2943), false, 12, new DateTime(2021, 6, 15, 0, 5, 52, 817, DateTimeKind.Local).AddTicks(2947), 17 },
                    { 16, new DateTime(2021, 6, 15, 0, 5, 52, 817, DateTimeKind.Local).AddTicks(2921), true, 9, new DateTime(2021, 6, 15, 0, 5, 52, 817, DateTimeKind.Local).AddTicks(2925), 16 },
                    { 14, new DateTime(2021, 6, 15, 0, 5, 52, 817, DateTimeKind.Local).AddTicks(2788), true, 13, new DateTime(2021, 6, 15, 0, 5, 52, 817, DateTimeKind.Local).AddTicks(2792), 16 },
                    { 13, new DateTime(2021, 6, 15, 0, 5, 52, 817, DateTimeKind.Local).AddTicks(2766), true, 6, new DateTime(2021, 6, 15, 0, 5, 52, 817, DateTimeKind.Local).AddTicks(2770), 6 },
                    { 12, new DateTime(2021, 6, 15, 0, 5, 52, 817, DateTimeKind.Local).AddTicks(2745), false, 20, new DateTime(2021, 6, 15, 0, 5, 52, 817, DateTimeKind.Local).AddTicks(2749), 2 },
                    { 11, new DateTime(2021, 6, 15, 0, 5, 52, 817, DateTimeKind.Local).AddTicks(2723), false, 13, new DateTime(2021, 6, 15, 0, 5, 52, 817, DateTimeKind.Local).AddTicks(2727), 4 },
                    { 15, new DateTime(2021, 6, 15, 0, 5, 52, 817, DateTimeKind.Local).AddTicks(2811), false, 18, new DateTime(2021, 6, 15, 0, 5, 52, 817, DateTimeKind.Local).AddTicks(2815), 3 },
                    { 9, new DateTime(2021, 6, 15, 0, 5, 52, 817, DateTimeKind.Local).AddTicks(2679), false, 9, new DateTime(2021, 6, 15, 0, 5, 52, 817, DateTimeKind.Local).AddTicks(2683), 21 },
                    { 8, new DateTime(2021, 6, 15, 0, 5, 52, 817, DateTimeKind.Local).AddTicks(2656), false, 13, new DateTime(2021, 6, 15, 0, 5, 52, 817, DateTimeKind.Local).AddTicks(2660), 20 },
                    { 7, new DateTime(2021, 6, 15, 0, 5, 52, 817, DateTimeKind.Local).AddTicks(2634), false, 20, new DateTime(2021, 6, 15, 0, 5, 52, 817, DateTimeKind.Local).AddTicks(2638), 19 },
                    { 6, new DateTime(2021, 6, 15, 0, 5, 52, 817, DateTimeKind.Local).AddTicks(2611), true, 1, new DateTime(2021, 6, 15, 0, 5, 52, 817, DateTimeKind.Local).AddTicks(2615), 17 },
                    { 10, new DateTime(2021, 6, 15, 0, 5, 52, 817, DateTimeKind.Local).AddTicks(2701), false, 11, new DateTime(2021, 6, 15, 0, 5, 52, 817, DateTimeKind.Local).AddTicks(2705), 5 },
                    { 5, new DateTime(2021, 6, 15, 0, 5, 52, 817, DateTimeKind.Local).AddTicks(2585), false, 14, new DateTime(2021, 6, 15, 0, 5, 52, 817, DateTimeKind.Local).AddTicks(2589), 9 },
                    { 4, new DateTime(2021, 6, 15, 0, 5, 52, 817, DateTimeKind.Local).AddTicks(2559), false, 14, new DateTime(2021, 6, 15, 0, 5, 52, 817, DateTimeKind.Local).AddTicks(2563), 5 },
                    { 3, new DateTime(2021, 6, 15, 0, 5, 52, 817, DateTimeKind.Local).AddTicks(2478), true, 6, new DateTime(2021, 6, 15, 0, 5, 52, 817, DateTimeKind.Local).AddTicks(2482), 14 },
                    { 2, new DateTime(2021, 6, 15, 0, 5, 52, 817, DateTimeKind.Local).AddTicks(2444), true, 2, new DateTime(2021, 6, 15, 0, 5, 52, 817, DateTimeKind.Local).AddTicks(2452), 10 }
                });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "Voluptas officia mollitia explicabo sit est repellendus. Voluptas ab possimus est doloribus sed consectetur assumenda eos. In libero excepturi explicabo doloremque.", new DateTime(2021, 6, 15, 0, 5, 52, 806, DateTimeKind.Local).AddTicks(2355), 23, new DateTime(2021, 6, 15, 0, 5, 52, 806, DateTimeKind.Local).AddTicks(2915) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 14, "Reiciendis est repudiandae eos laudantium repellendus eveniet consequatur nesciunt quod.\nOmnis necessitatibus est iste.\nAutem tempore et assumenda voluptas aliquam odio.\nIure similique ipsa incidunt natus blanditiis nesciunt excepturi et.\nDolor porro dolores eveniet ratione quia eius vel est qui.", new DateTime(2021, 6, 15, 0, 5, 52, 806, DateTimeKind.Local).AddTicks(5354), 24, new DateTime(2021, 6, 15, 0, 5, 52, 806, DateTimeKind.Local).AddTicks(5375) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 19, "Et ut quaerat nihil itaque eaque est. Magni voluptatum eveniet quod. Magnam cumque qui facilis impedit maiores rerum quo. Eveniet non deserunt unde laudantium facilis. Laborum voluptas repellendus. Optio inventore aut ut aut.", new DateTime(2021, 6, 15, 0, 5, 52, 806, DateTimeKind.Local).AddTicks(5666), 23, new DateTime(2021, 6, 15, 0, 5, 52, 806, DateTimeKind.Local).AddTicks(5673) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 9, "unde", new DateTime(2021, 6, 15, 0, 5, 52, 806, DateTimeKind.Local).AddTicks(6053), 21, new DateTime(2021, 6, 15, 0, 5, 52, 806, DateTimeKind.Local).AddTicks(6067) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 3, "totam", new DateTime(2021, 6, 15, 0, 5, 52, 806, DateTimeKind.Local).AddTicks(6177), 32, new DateTime(2021, 6, 15, 0, 5, 52, 806, DateTimeKind.Local).AddTicks(6183) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "Repudiandae quibusdam voluptas optio odit qui qui. Reprehenderit in beatae. Occaecati quo aut. Ut adipisci deserunt soluta quo qui. Rerum nostrum reprehenderit. Minima consequuntur doloribus suscipit id ut.", new DateTime(2021, 6, 15, 0, 5, 52, 806, DateTimeKind.Local).AddTicks(6358), 22, new DateTime(2021, 6, 15, 0, 5, 52, 806, DateTimeKind.Local).AddTicks(6363) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 19, "Tenetur expedita ipsum eligendi id nostrum reiciendis nihil atque quo. Voluptatibus officia recusandae ipsam. Eligendi sit cum a. Quo reiciendis qui maiores in et.", new DateTime(2021, 6, 15, 0, 5, 52, 806, DateTimeKind.Local).AddTicks(6536), 27, new DateTime(2021, 6, 15, 0, 5, 52, 806, DateTimeKind.Local).AddTicks(6547) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 19, "Sed et aut labore recusandae excepturi qui pariatur eum.", new DateTime(2021, 6, 15, 0, 5, 52, 806, DateTimeKind.Local).AddTicks(7246), 39, new DateTime(2021, 6, 15, 0, 5, 52, 806, DateTimeKind.Local).AddTicks(7262) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "Qui ea in quam. Reprehenderit occaecati ipsam. Officia labore soluta porro nihil suscipit. Quidem sed quae. Sit sint et optio vel dolorum unde atque nulla. Ut aut numquam.", new DateTime(2021, 6, 15, 0, 5, 52, 806, DateTimeKind.Local).AddTicks(7523), 27, new DateTime(2021, 6, 15, 0, 5, 52, 806, DateTimeKind.Local).AddTicks(7530) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 2, "Repellendus quae laboriosam aut id eum repellat magnam autem iusto. Dolor ut sit vero voluptates sed tempora. Nihil ipsa atque voluptas eaque totam voluptate quae et. Ea et laboriosam quia quo placeat. Excepturi numquam enim. Optio consequuntur dolorem aut.", new DateTime(2021, 6, 15, 0, 5, 52, 806, DateTimeKind.Local).AddTicks(7746), 40, new DateTime(2021, 6, 15, 0, 5, 52, 806, DateTimeKind.Local).AddTicks(7752) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { "Corporis adipisci et quia eos aut corporis sed corporis suscipit. Suscipit similique repellendus iure. Ipsum in iste qui vero in et.", new DateTime(2021, 6, 15, 0, 5, 52, 806, DateTimeKind.Local).AddTicks(7852), new DateTime(2021, 6, 15, 0, 5, 52, 806, DateTimeKind.Local).AddTicks(7857) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, "repudiandae", new DateTime(2021, 6, 15, 0, 5, 52, 806, DateTimeKind.Local).AddTicks(7892), 34, new DateTime(2021, 6, 15, 0, 5, 52, 806, DateTimeKind.Local).AddTicks(7905) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "ea", new DateTime(2021, 6, 15, 0, 5, 52, 806, DateTimeKind.Local).AddTicks(7970), 33, new DateTime(2021, 6, 15, 0, 5, 52, 806, DateTimeKind.Local).AddTicks(7974) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 10, "Non dolore dolores et.", new DateTime(2021, 6, 15, 0, 5, 52, 806, DateTimeKind.Local).AddTicks(8044), 37, new DateTime(2021, 6, 15, 0, 5, 52, 806, DateTimeKind.Local).AddTicks(8048) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 14, "Fugit eveniet magni nisi iusto ut et corrupti tempora et. Ut nostrum cumque. Magni ut facilis rem ut non similique et.", new DateTime(2021, 6, 15, 0, 5, 52, 806, DateTimeKind.Local).AddTicks(8144), 22, new DateTime(2021, 6, 15, 0, 5, 52, 806, DateTimeKind.Local).AddTicks(8149) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 11, "voluptas", new DateTime(2021, 6, 15, 0, 5, 52, 806, DateTimeKind.Local).AddTicks(8174), 21, new DateTime(2021, 6, 15, 0, 5, 52, 806, DateTimeKind.Local).AddTicks(8178) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, "Adipisci accusantium et.\nDebitis similique eius consectetur ut sint in quia.", new DateTime(2021, 6, 15, 0, 5, 52, 806, DateTimeKind.Local).AddTicks(8281), 28, new DateTime(2021, 6, 15, 0, 5, 52, 806, DateTimeKind.Local).AddTicks(8286) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 2, "Recusandae et quibusdam voluptas ut rerum. Qui quae est quaerat numquam voluptas aut. Quos aperiam impedit voluptatem eveniet doloribus aliquam. Molestiae minima ut perferendis dolorem exercitationem. Ex quaerat omnis unde culpa vero laboriosam qui.", new DateTime(2021, 6, 15, 0, 5, 52, 806, DateTimeKind.Local).AddTicks(8486), 25, new DateTime(2021, 6, 15, 0, 5, 52, 806, DateTimeKind.Local).AddTicks(8492) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 19, "Eum nulla sapiente reiciendis et.", new DateTime(2021, 6, 15, 0, 5, 52, 806, DateTimeKind.Local).AddTicks(8540), 39, new DateTime(2021, 6, 15, 0, 5, 52, 806, DateTimeKind.Local).AddTicks(8545) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "Distinctio est dolores cum totam quis ab.\nRecusandae autem aut.\nConsequatur alias iusto numquam qui.", new DateTime(2021, 6, 15, 0, 5, 52, 806, DateTimeKind.Local).AddTicks(8627), 33, new DateTime(2021, 6, 15, 0, 5, 52, 806, DateTimeKind.Local).AddTicks(8631) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 12, new DateTime(2021, 6, 15, 0, 5, 52, 648, DateTimeKind.Local).AddTicks(2988), "Savanah.Hintz@gmail.com", "NH3MHE0xzLVmbG9seqjX+w1RHGnL7dzPUI98i2eE46I=", "c39iLvnadzwI8qK+hbtmx1vzuUOUq/V7tktwViEmuV8=", new DateTime(2021, 6, 15, 0, 5, 52, 648, DateTimeKind.Local).AddTicks(3564), "Sincere.Bernier23" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 3, new DateTime(2021, 6, 15, 0, 5, 52, 655, DateTimeKind.Local).AddTicks(7452), "Brielle23@yahoo.com", "/HiK/22/VNtw7lXMBM6bxYUzdroGba4K5+12htvyDpk=", "6xSxZdQxsbRfcF0uMnurvxQzJMdCr9ksRqjCCaycbNU=", new DateTime(2021, 6, 15, 0, 5, 52, 655, DateTimeKind.Local).AddTicks(7470), "Berry_Rath65" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2021, 6, 15, 0, 5, 52, 662, DateTimeKind.Local).AddTicks(9789), "Lenna_Hayes@gmail.com", "yWIx/pBV/DPGpKfwUUV2xZ22K5bjMjoGnMhAE+uUwvw=", "FPsR3vRTzqUXtWDtE7NBR6obP7CySmDM/YV1CPK0ZlU=", new DateTime(2021, 6, 15, 0, 5, 52, 662, DateTimeKind.Local).AddTicks(9801), "London88" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 19, new DateTime(2021, 6, 15, 0, 5, 52, 670, DateTimeKind.Local).AddTicks(2324), "Kamren30@hotmail.com", "7fUFnyGZW3Z8fBzncq2Mgj7g8sMF26mSQwZkF39ma3E=", "I9bZRDv+0PAOzpVUYzGcOnF+4nOYB+5s2se27TK26y4=", new DateTime(2021, 6, 15, 0, 5, 52, 670, DateTimeKind.Local).AddTicks(2339), "Rashawn.Ebert" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2021, 6, 15, 0, 5, 52, 677, DateTimeKind.Local).AddTicks(5595), "Julia30@yahoo.com", "BQYcgSflToIT1sabo5+eyQdAaOBaXf2YpHks2E4ydU8=", "fAhDuqS8iHQ7lZa+MVeyf7AwYfyJRweSvckSRDm6tk8=", new DateTime(2021, 6, 15, 0, 5, 52, 677, DateTimeKind.Local).AddTicks(5623), "Demarcus_Connelly" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2021, 6, 15, 0, 5, 52, 684, DateTimeKind.Local).AddTicks(8274), "Adaline.Zulauf5@yahoo.com", "41mp1exNDt3yDpk5g6YYZM3ehki4J0PONJbsfc2AcjI=", "xZtH7yrOGWoNtWAt7bH/WuGxrF3bbaVQGvmbV4e5L+E=", new DateTime(2021, 6, 15, 0, 5, 52, 684, DateTimeKind.Local).AddTicks(8290), "Arno.Mueller31" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2021, 6, 15, 0, 5, 52, 692, DateTimeKind.Local).AddTicks(4170), "Mohammed92@yahoo.com", "GIG695b9CWQnjX0sRSHzQo/nRZeQbDwNAtBn0eZ8bOE=", "uWI6nxkchvSofAHqU1frmQdK2bs/k8p79UNGi/+2+kk=", new DateTime(2021, 6, 15, 0, 5, 52, 692, DateTimeKind.Local).AddTicks(4196), "Jocelyn_Ryan" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 16, new DateTime(2021, 6, 15, 0, 5, 52, 699, DateTimeKind.Local).AddTicks(6161), "Kyler_Herzog32@yahoo.com", "i4dJVWtY139ynvWydrIf/lCasOm2WncaRaXFfwPazbQ=", "yfGsx2JBzqSWC4ovv6bgaKf9nS6a8L/qp/Osz5cxvrQ=", new DateTime(2021, 6, 15, 0, 5, 52, 699, DateTimeKind.Local).AddTicks(6172), "Jermey.Becker" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2021, 6, 15, 0, 5, 52, 707, DateTimeKind.Local).AddTicks(1601), "Iva77@gmail.com", "rI6eAEY9eeaANCG5/KBJBW2Apt9Q9tvDchyueqdgHzY=", "wDxIZCWkI42SVeK/HHIySAklwd73/36jS8zWzGIyMrU=", new DateTime(2021, 6, 15, 0, 5, 52, 707, DateTimeKind.Local).AddTicks(1633), "Sierra26" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 1, new DateTime(2021, 6, 15, 0, 5, 52, 714, DateTimeKind.Local).AddTicks(4026), "Concepcion82@yahoo.com", "krnhWv3gzZNlABRI+NMUN1KM1BKxJssT6EYscwXdJJ8=", "GgRkl9p80ZZT/UVpEJnlsEKUeENw3wFteajOHK1hV8w=", new DateTime(2021, 6, 15, 0, 5, 52, 714, DateTimeKind.Local).AddTicks(4039), "Marlin.Streich" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2021, 6, 15, 0, 5, 52, 721, DateTimeKind.Local).AddTicks(9744), "Samantha.Bartoletti92@hotmail.com", "aZPly/ePz3Exj2QAZ6NR5yhopxH8xC5bFZ5TY65SlZ8=", "OBecom6TL2N5W5uwwLq8OtCI7l3cmVLBeDMJra72hbs=", new DateTime(2021, 6, 15, 0, 5, 52, 721, DateTimeKind.Local).AddTicks(9765), "Andreanne.Towne11" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2021, 6, 15, 0, 5, 52, 729, DateTimeKind.Local).AddTicks(1941), "Bulah60@yahoo.com", "vlAcOY8VftmQUAgPksWL8QpgezmOUp2eqABy+TEg8XE=", "zFAOE0B1bdE1Ops6CMwbnvf9hLccp9dH+oovirRGelg=", new DateTime(2021, 6, 15, 0, 5, 52, 729, DateTimeKind.Local).AddTicks(1952), "Geoffrey45" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2021, 6, 15, 0, 5, 52, 736, DateTimeKind.Local).AddTicks(6110), "Lola49@hotmail.com", "34ryZSBBhRvixX+CJpPIB72f4L8yaw+uII7mXR/902s=", "vXm04+ZzCfngxjZc3eQeVQiowYjZWlJlJWUTB/DjzCE=", new DateTime(2021, 6, 15, 0, 5, 52, 736, DateTimeKind.Local).AddTicks(6124), "Camilla.Smith" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2021, 6, 15, 0, 5, 52, 744, DateTimeKind.Local).AddTicks(1154), "Emile.Towne4@gmail.com", "qdzSEA+nZy7iugITFwn0RAiz/amhZmNdFd/IHJ54iRw=", "9tKFsQDRY47lI+GlcxPZRt2vTathacze3nfr1dnGotc=", new DateTime(2021, 6, 15, 0, 5, 52, 744, DateTimeKind.Local).AddTicks(1168), "Selena.Sawayn46" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2021, 6, 15, 0, 5, 52, 751, DateTimeKind.Local).AddTicks(3646), "Jo_Bogan@gmail.com", "BIRz1xSU9MgasNRB/4qSgttpu8NRKPrU6kRNgE9MbjI=", "TLpXhzf0PxQat44jCZxUI8PVuxM9fB03S2MrVHLGMLw=", new DateTime(2021, 6, 15, 0, 5, 52, 751, DateTimeKind.Local).AddTicks(3655), "Krystal.McGlynn32" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2021, 6, 15, 0, 5, 52, 758, DateTimeKind.Local).AddTicks(8823), "Matteo98@yahoo.com", "irLmfrHVQHaa8pYOQh1WIz1H7di4mVmxccDVzR/ZMbg=", "VHuaL3t4jjJkLB9IQua8mqRwvRZoYy9ledN/mc42Enk=", new DateTime(2021, 6, 15, 0, 5, 52, 758, DateTimeKind.Local).AddTicks(8847), "Mandy96" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2021, 6, 15, 0, 5, 52, 766, DateTimeKind.Local).AddTicks(2031), "Moses30@gmail.com", "1kdpmzUehZDr2J7zFw54K1Ts7WcrBMw+i+kB5z+wh8A=", "QiNCxsF1jVVYaW4c3VYReDFH1HlZpiBQ0wGwIWj5PqI=", new DateTime(2021, 6, 15, 0, 5, 52, 766, DateTimeKind.Local).AddTicks(2042), "Philip_Ortiz" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2021, 6, 15, 0, 5, 52, 773, DateTimeKind.Local).AddTicks(8980), "Karolann.Rippin@yahoo.com", "zsQMyhRJb4mC47MQxh+r+hRBllejvTEVdMTd7ZvOVK0=", "y0G3eFmP0SjtvzYiQBq/BKWvna1qYHcKM2kLPzgZtCA=", new DateTime(2021, 6, 15, 0, 5, 52, 773, DateTimeKind.Local).AddTicks(9015), "Lysanne_Schulist" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2021, 6, 15, 0, 5, 52, 781, DateTimeKind.Local).AddTicks(4495), "Columbus31@gmail.com", "74XrggroSwVjYNd1oZKuQPPBo2iH+90oCfXOvpLv3Wk=", "3PVQ/3aBn4YTeqKrKHwWmazqdPsy1amRo0wQI2mGpe8=", new DateTime(2021, 6, 15, 0, 5, 52, 781, DateTimeKind.Local).AddTicks(4515), "Kathlyn_Mohr55" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 3, new DateTime(2021, 6, 15, 0, 5, 52, 789, DateTimeKind.Local).AddTicks(2583), "Maxine39@yahoo.com", "OHf9im1bkPit1qX8nC9xUjbK3B1Lu4Imesgke4jo2NE=", "ZMRqe7BR2iO4TCAmuZDvVL+/5amzOXW3riL5i7tSThM=", new DateTime(2021, 6, 15, 0, 5, 52, 789, DateTimeKind.Local).AddTicks(2640), "Larry.Vandervort2" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 15, 0, 5, 52, 796, DateTimeKind.Local).AddTicks(6669), "xHlNGxf89TmbGoBB5Otbar+8+S7sNzHylPs/dQFxpUw=", "kwctRtp29UtWP47h0sf5/dm/URckM9WZg+TkvDxQkV0=", new DateTime(2021, 6, 15, 0, 5, 52, 796, DateTimeKind.Local).AddTicks(6669) });
        }
    }
}
