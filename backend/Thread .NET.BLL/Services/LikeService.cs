﻿using AutoMapper;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.DAL.Context;

namespace Thread_.NET.BLL.Services
{
    public sealed class LikeService : BaseService
    {
        public LikeService(ThreadContext context, IMapper mapper) : base(context, mapper) { }

        public async Task ReactPost(NewReactionDTO reaction)
        {
            var oldReact = _context.PostReactions.Where(x => x.UserId == reaction.UserId && x.PostId == reaction.EntityId).FirstOrDefault();

            if (oldReact != null)
            {
                oldReact.ReactionMark = (DAL.Entities.Abstract.ReactionMark)reaction.ReactionMark;

                _context.Update(oldReact);
                await _context.SaveChangesAsync();

                return;
            }

            _context.PostReactions.Add(new DAL.Entities.PostReaction
            {
                PostId = reaction.EntityId,
                ReactionMark = (DAL.Entities.Abstract.ReactionMark)reaction.ReactionMark,
                UserId = reaction.UserId
            });

            await _context.SaveChangesAsync();
        }
    }
}
