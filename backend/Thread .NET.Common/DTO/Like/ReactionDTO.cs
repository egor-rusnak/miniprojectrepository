﻿using Thread_.NET.Common.DTO.User;

namespace Thread_.NET.Common.DTO.Like
{
    public enum ReactionMark
    {
        Like,
        Dislike,
        Restricted
    }
    public sealed class ReactionDTO
    {
        public ReactionMark ReactionMark { get; set; }
        public UserDTO User { get; set; }
    }
}
