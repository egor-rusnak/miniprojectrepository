﻿namespace Thread_.NET.Common.DTO.Like
{
    public sealed class NewReactionDTO
    {
        public int EntityId { get; set; }
        public ReactionMark ReactionMark { get; set; }
        public int UserId { get; set; }
    }
}
