import { Component, Input, OnDestroy } from '@angular/core';
import { Post } from '../../models/post/post';
import { AuthenticationService } from '../../services/auth.service';
import { AuthDialogService } from '../../services/auth-dialog.service';
import { empty, Observable, Subject } from 'rxjs';
import { DialogType } from '../../models/common/auth-dialog-type';
import { LikeService } from '../../services/like.service';
import { NewComment } from '../../models/comment/new-comment';
import { CommentService } from '../../services/comment.service';
import { User } from '../../models/user';
import { Comment } from '../../models/comment/comment';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { SnackBarService } from '../../services/snack-bar.service';
import { ThisReceiver } from '@angular/compiler';
import { PostService } from 'src/app/services/post.service';
import { GyazoService } from 'src/app/services/gyazo.service';
import { ReactionMark } from 'src/app/models/reactions/reaction';

@Component({
    selector: 'app-post',
    templateUrl: './post.component.html',
    styleUrls: ['./post.component.sass']
})
export class PostComponent implements OnDestroy {
    @Input() public post: Post;
    @Input() public currentUser: User;

    public get reactionMark(): typeof ReactionMark {
        return ReactionMark;
      }

    public showComments = false;
    public newComment = {} as NewComment;
    public updatePost : Post;
    public imageUrl: string;
    public imageFile: File;
    public isModify:Boolean = false;
    private unsubscribe$ = new Subject<void>();

    public constructor(
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private likeService: LikeService,
        private commentService: CommentService,
        private snackBarService: SnackBarService,
        private postService:PostService,
        private gyazoService: GyazoService
    ) { }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public toggleComments() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe((user) => {
                    if (user) {
                        this.currentUser = user;
                        this.showComments = !this.showComments;
                    }
                });
            return;
        }

        this.showComments = !this.showComments;
    }

    public checkUser(): Boolean {
        if (!this.currentUser) return false;
        return this.post.author.id===this.currentUser.id;
    }

    public removePost(){
        if(!this.checkUser()) return;
        this.postService.removePost(this.post.id).subscribe(()=>{
            this.snackBarService.showUsualMessage("Post was removed");
        });
    }

    public modifyPost(){
        if(!this.checkUser()) return;
        this.updatePost=Object.assign({},this.post);
        this.isModify=true;
    }

    public loadImage(target: any) {
        this.imageFile = target.files[0];

        if (!this.imageFile) {
            target.value = '';
            return;
        }

        if (this.imageFile.size / 1000000 > 5) {
            target.value = '';
            this.snackBarService.showErrorMessage(`Image can't be heavier than ~5MB`);
            return;
        }

        const reader = new FileReader();
        reader.addEventListener('load', () => (this.imageUrl = reader.result as string));
        reader.readAsDataURL(this.imageFile);
    }

    public removeImage() {
        this.imageUrl = undefined;
        this.imageFile = undefined;
    }

    public savePost(){
        this.post.body = this.updatePost.body;
        this.post.previewImage = this.updatePost.previewImage;
        this.postService.updatePost(this.post).subscribe(e=>{
            this.isModify=false;
            this.snackBarService.showUsualMessage("Post successfully updated");
        },(error)=>{
            this.isModify=false;
            this.snackBarService.showErrorMessage("Error while updating post");
        });

    }

    public reactPost(mark: ReactionMark) {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.likePost(this.post, userResp, mark)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((post) => (this.post = post));

            return;
        }

        this.likeService
            .likePost(this.post, this.currentUser,mark)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((post) => (this.post = post));
    }

    public sendComment() {
        this.newComment.authorId = this.currentUser.id;
        this.newComment.postId = this.post.id;

        this.commentService
            .createComment(this.newComment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.post.comments = this.sortCommentArray(this.post.comments.concat(resp.body));
                        this.newComment.body = undefined;
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }
    public get dislikes():number {
        return this.post.reactions.filter(e=>e.reactionMark==ReactionMark.Dislike).length;
    }
    public get likes():number {
        return this.post.reactions.filter(e=>e.reactionMark==ReactionMark.Like).length;
    }

    private sortCommentArray(array: Comment[]): Comment[] {
        return array.sort((a, b) => +new Date(b.createdAt) - +new Date(a.createdAt));
    }
}
