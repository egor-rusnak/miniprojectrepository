import { User } from '../user';
export enum ReactionMark{
    Like=0,
    Dislike=1,
    Restricted=2
}
export interface Reaction {
    reactionMark: ReactionMark;
    user: User;
}
