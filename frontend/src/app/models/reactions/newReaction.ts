import { ReactionMark } from "./reaction";

export interface NewReaction {
    entityId: number;
    reactionMark: ReactionMark;
    userId: number;
}
