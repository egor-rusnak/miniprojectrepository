import { Injectable } from '@angular/core';
import { AuthenticationService } from './auth.service';
import { Post } from '../models/post/post';
import { NewReaction } from '../models/reactions/newReaction';
import { PostService } from './post.service';
import { User } from '../models/user';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { ReactionMark } from '../models/reactions/reaction';
import { MatExpansionPanelDescription } from '@angular/material/expansion';

@Injectable({ providedIn: 'root' })
export class LikeService {
    public constructor(private authService: AuthenticationService, private postService: PostService) {}

    public likePost(post: Post, currentUser: User, mark: ReactionMark) {
        const innerPost = post;

        let resultMark = mark;
        let existingMark = innerPost.reactions.find(e=>e.user.id===currentUser.id);
        if(existingMark){
            if(resultMark == existingMark.reactionMark)
                resultMark = ReactionMark.Restricted;
        }

        const reaction: NewReaction = {
            entityId: innerPost.id,
            reactionMark: resultMark,
            userId: currentUser.id
        };
        // update current array instantly
        let hasReaction = innerPost.reactions.some((x) => x.user.id === currentUser.id);


        innerPost.reactions = hasReaction
            ? innerPost.reactions.filter((x) => x.user.id !== currentUser.id)
            : innerPost.reactions.concat({ reactionMark: resultMark, user: currentUser });
        hasReaction = innerPost.reactions.some((x) => x.user.id === currentUser.id);

        return this.postService.likePost(reaction).pipe(
            map(() => innerPost),
            catchError(() => {
                // revert current array changes in case of any error
                innerPost.reactions = hasReaction
                    ? innerPost.reactions.filter((x) => x.user.id !== currentUser.id)
                    : innerPost.reactions.concat({ reactionMark: resultMark, user: currentUser });

                return of(innerPost);
            })
        );
    }
}
